extern crate serde;
extern crate serde_json;

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub struct Person {
    pub nombre: String,
    pub apellido: String,
    pub edad: u8,
    pub email: String,
}

