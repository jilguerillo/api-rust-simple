
extern crate serde_json;
extern crate tokio;
extern crate warp;

use std::fs;
use warp::Filter;

use serde::Serialize;
use serde::Deserialize;

#[tokio::main]
async fn main() {
    // Crea un filtro que se encargará de manejar la ruta /datos
    let datos = warp::path("people").and(warp::get()).map(obtener_datos);

    // Crea el servidor y escucha en el puerto 8000
    let routes = datos.with(warp::log("api"));
    warp::serve(routes).run(([0, 0, 0, 0], 8000)).await;
}

// Esta es la función que se encargará de leer el archivo JSON y devolver los datos
fn obtener_datos() -> impl warp::Reply {
    let ruta = "datos.json";
    println!("Intentando leer el archivo en la ruta: {}", ruta);

    let contents = match fs::read_to_string(ruta) {
        Ok(contents) => contents,
        Err(error) => return manejador_de_errores(error),
    };

    let datos: Datos = match serde_json::from_str(&contents) {
        Ok(datos) => datos,
        Err(error) => return manejador_de_errores(error),
    };

    // Devuelve los datos como una respuesta JSON
    warp::reply::json(&datos)
}


#[derive(Deserialize, Serialize)]
struct Datos {
    nombre: String,
    apellido: String,
    edad: u32,
    email: String,
}

fn manejador_de_errores(error: std::io::Error) -> impl warp::Reply {
    // Si el error es de tipo "No such file or directory", devuelve los datos en duro
    if error.kind() == std::io::ErrorKind::NotFound {
        let datos = Datos {
            nombre: "Juan".to_string(),
            apellido: "Pérez".to_string(),
            edad: 30,
            email: "juan@example.com".to_string(),
        };
        return warp::reply::json(&datos);
    }

    // Si el error es de otro tipo, devuelve una respuesta HTTP con el código de error 500 y el mensaje de error
    let mensaje = format!("Se ha producido un error: {}", error);
    warp::reply::with_status(warp::reply::json(&mensaje), warp::http::StatusCode::INTERNAL_SERVER_ERROR)
}




